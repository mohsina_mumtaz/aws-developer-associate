# Certified AWS Associate Developer Notes

### 2021 AWS developer associate exam 

## Table of contents

- AWS Fundamentals
    - [IAM: Identity Access & Management](exam_materials/1-aws-fundamentals/iam.md)
    - [EC2: Virtual Machines](exam_materials/1-aws-fundamentals/ec2.md)
    - [Security Groups](exam_materials/1-aws-fundamentals/security-groups.md)
    - [ELB: Elastic Load Balancers](exam_materials/1-aws-fundamentals/elb.md)
    - [ASG: Auto Scaling Group](exam_materials/1-aws-fundamentals/asg.md)
    - [EBS: Elastic Block Store](exam_materials/1-aws-fundamentals/ebs.md)
    - [RDS: Relational Database Service](exam_materials/1-aws-fundamentals/rds.md)
    - [Route 53](exam_materials/1-aws-fundamentals/route53.md)
    - [ElastiCache](exam_materials/1-aws-fundamentals/elasticache.md)
    - [VPC: Virtual Private Cloud](exam_materials/1-aws-fundamentals/vpc.md)
    - [S3 Buckets](exam_materials/1-aws-fundamentals/s3.md)

- AWS Deep Dive
    - [CLI: Command Line Interface](exam_materials/2-aws-deep-dive/cli.md)
    - [SDK: Software Development Kit](exam_materials/2-aws-deep-dive/sdk.md)
    - [Elastic Beanstalk](exam_materials/2-aws-deep-dive/elastic-beanstalk.md)
    - [CICD: Continuous Integration and Deployment](exam_materials/2-aws-deep-dive/cicd/cicd.md)
        - [CodeCommit](exam_materials/2-aws-deep-dive/cicd/codecommit.md)
        - [CodePipeline](exam_materials/2-aws-deep-dive/cicd/codepipeline.md)
        - [CodeBuild](exam_materials/2-aws-deep-dive/cicd/codebuild.md)
        - [CodeDeploy](exam_materials/2-aws-deep-dive/cicd/codedeploy.md)
    - [CloudFormation](exam_materials/2-aws-deep-dive/cloudformation/cloudformation.md)
    - [CloudWatch](exam_materials/2-aws-deep-dive/monitoring-and-audit/cloudwatch.md)
    - [Integration and Messaging](exam_materials/2-aws-deep-dive/integration-and-messaging/0-intro.md)
        - [SQS](exam_materials/2-aws-deep-dive/integration-and-messaging/1-sqs.md)
        - [SNS](exam_materials/2-aws-deep-dive/integration-and-messaging/2-sns.md)
        - [Kinesis](exam_materials/2-aws-deep-dive/integration-and-messaging/3-kinesis.md)

- [YAML](exam_materials/2-aws-deep-dive/yaml.md)

- [AWS Serverless](exam_materials/3-aws-serverless/serverless.md)
  - [Lambda](exam_materials/3-aws-serverless/lambda.md)
  - [DynamoDB](exam_materials/3-aws-serverless/dynamodb.md)
  - [API Gateway](exam_materials/3-aws-serverless/apigateway.md)
  - [SAM](exam_materials/3-aws-serverless/sam.md)
  - [Cognito](exam_materials/3-aws-serverless/cognito.md)
  - [Step Functions](exam_materials/3-aws-serverless/stepfunctions.md)
  - [AppSync](exam_materials/3-aws-serverless/appsync.md)

- Docker based AWS services
  - ECS: Elastic Container Service
  - Elastic Container Registry
  - Fargate

- [Exam Preperation](#exam-preparation)


## Exam Preparation

- Exam details
    - Two question types:
        - Multiple Choice
        - Multiple response
    - Minimum passing score: 720/1000
    - Domains:
        - Deployment: CICD, Beanstalk, Serverless
        - Security: each service deep-dive + dedicated section
        - Development with AWS Services: Serverless, API, SDK, & CLI
        - Refactoring: Understand all the AWS services for the best migration
        - Monitoring and Troubleshooting: CloudWAtch, CloudTrail, X-Ray

    - Exam Guide:
        - [Certified Developer - Associate Exam PDF](https://d1.awsstatic.com/training-and-certification/docs-dev-associate/AWS_Certified_Developer_Associate-Exam_Guide_EN_1.4.pdf)

- EC2 + IAM Exam Checklist
  * Know how to SSH into EC2 (and change .pem file permissions) 
  * Know how to properly use security groups 
  * Know the fundamental differences between private vs public vs elastic IP 
  * Know how to use User Data to customize your instance at boot time 
  * Know that you can build custom AMI to enhance your OS 
  * EC2 instances are billed by the second and can be easily created and thrown away, welcome to the cloud! 
  Maybe on Exam:
  * Availability zones are in geographically isolated data centers
  * IAM users are NOT defined on a per-region basis
  * If you are getting a permission error exception when trying to SSH into your linux instance, then the key is missing chmod 400 permissions
  * If you are getting a network timeout when trying to SSH into your EC2 instance, then your security groups are misconfigured
  * Security groups reference IP address, CIDR block, Security group, but NOT DNS name


